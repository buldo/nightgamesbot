package ngsbot

import (
	"github.com/jinzhu/gorm"
	"github.com/rockneurotiko/go-tgbot"
	"bitbucket.org/buldo/nightgamesbot/clients/fxclient"
	"encoding/json"
	"bitbucket.org/buldo/nightgamesbot/clients"
	"strings"
)

type clientSettings struct {
	gorm.Model
	Name string
	Type string
	Settings string
}

func (bot *NgsBot)processClientsCommands(msg *tgbot.MessageWithUpdateID) {
	parts := strings.Split(strings.ToLower(*msg.Msg.Text)," ")
	if(len(parts) == 1) {
		bot.tgBot.SimpleSendMessage(msg.Msg, "Доступные команды list, add, remove")
	} else {
		switch parts[1] {
		case "list":
			bot.clients_list()
			break
		case "add":

			break
		case "remove":
			break
		}
	}

}


func (bot *NgsBot)reloadClients() {
	var clients []clientSettings;
	bot.database.Find(&clients)
	for _,element := range clients {
		switch element.Type {
		case fxclient.ClientName :
			var fxSettings fxclient.FxClientSettings
			json.Unmarshal([]byte(element.Settings), &fxSettings)
			bot.clients = append(bot.clients, fxclient.New(fxSettings))
			break;
		}
	}
}

func (bot *NgsBot)setActiveClient(client *clients.GameClient) {
	bot.activeClient = client
}

func (bot *NgsBot)clients_list()  {
	var clients []clientSettings;
	bot.database.Find(&clients)
	var output []string
	output = append(output, "Настроенные клиенты")
	for _,element := range clients {
		output = append(output, element.Name)
	}
	bot.tgBot.SimpleSendMessage(strings.Join(output, "\n"))
}

func (bot *NgsBot)clients_add(splited []string)  {
	
}