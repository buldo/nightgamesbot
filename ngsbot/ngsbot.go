package ngsbot

import (
	"github.com/rockneurotiko/go-tgbot"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"strings"
	"bitbucket.org/buldo/nightgamesbot/clients"
)

type NgsBot struct {
	tgBot        *tgbot.TgBot
	config       BotConfig
	database     *gorm.DB
	clients      []clients.GameClient
	activeClient *clients.GameClient
}

type methodsRouter struct {
	methods []*method
}

type method struct {
	name string

}

func NewBot(config BotConfig, tgBot *tgbot.TgBot) *NgsBot{
	bot := new(NgsBot)
	bot.config = config
	bot.tgBot = tgBot

	// Настройка базы
	db, err := gorm.Open("postgres", config.PostgresConnectionString)
	if err != nil {
		panic("failed to connect database")
	}
	bot.database = db
	bot.database.AutoMigrate(user{},clientSettings{})

	// Загружаем клиентов
	bot.reloadClients()



	return bot
}

// MessageHandler will be the custom handler
func (self *NgsBot)MessageHandler(Incoming <-chan tgbot.MessageWithUpdateID) {
	for {
		input := <-Incoming

		if input.Msg.Text != nil {
			if self.verifyAccess(input.Msg.From.ID) {
				// Подтверждённый пользователь
				if(strings.HasPrefix(*input.Msg.Text,"/")) {
					// Выполнение комманды
					switch  getCommand(input.Msg.Text) {
					case "help" :
						self.tgBot.SimpleSendMessage(input.Msg, "Доступные команды\n/clients")
						break;
						case "clients" :
							self.processClientsCommands(&input)
							break;

					}
				} else {
					// Выполнение действия по умолчанию
				}
			} else {
				// Необхдима регистрация
				self.processNotVerifiedMsg(input)
			}

			// Сначала проверка на логин
			//input.Msg.

			//text := handleMessageText(*input.Msg.Text, input.Msg)
			//if text != "" {
			//	nmsg, err := bot.SimpleSendMessage(input.Msg, text)
			//	if err != nil {
			//		fmt.Println(err)
			//		continue
			//	}
			//	fmt.Println(nmsg.String())
			//}
		}
	}
}

func getCommand(text *string) string  {
	return strings.ToLower(strings.TrimLeft(strings.Split(*text, " ")[0], "/"))
}