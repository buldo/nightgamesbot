package ngsbot

type BotConfig struct {
	MasterPassword string
	UserPassword string
	PostgresConnectionString string
}
