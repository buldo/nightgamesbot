package ngsbot

import (
	"github.com/jinzhu/gorm"
	"github.com/rockneurotiko/go-tgbot"
)

type user struct {
	gorm.Model
	TelegramId int
	UserName string
	IsAuthenticated bool
}

func (self *NgsBot)verifyAccess(tgUserId int) bool {
	var usr user
	tempdb := self.database.First(&usr, user{TelegramId:tgUserId})

	if(tempdb.Error != nil) {
		return false
	}
	if(usr.IsAuthenticated) {
		return true
	}

	return false
}

func (self *NgsBot)processNotVerifiedMsg(msg tgbot.MessageWithUpdateID)  {

	var usr user

	id := msg.Msg.From.ID
	if(self.database.First(&usr, user{TelegramId:id}).RowsAffected == 0) {
		name := *msg.Msg.From.Username
		self.database.Create(&user{UserName:name, TelegramId:id})
	}

	if *msg.Msg.Text == self.config.MasterPassword {
		usr.IsAuthenticated = true
		self.database.Save(usr)
		self.tgBot.SimpleSendMessage(msg.Msg, "Добро пожаловать")
	} else {
		self.tgBot.SimpleSendMessage(msg.Msg, "Введите пароль")
	}
}