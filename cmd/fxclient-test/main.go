package main

import ("os"
	"fmt"
	"bitbucket.org/buldo/nightgamesbot/clients/fxclient"
)

const urlString string = "baseUrl"
const usernameString string = "username"
const passwordString string = "password"

func main() {
	fxSettings := fxclient.FxClientSettings{BaseUrl:os.Getenv(urlString), Login:os.Getenv(usernameString), Password:os.Getenv(usernameString)}
	client := fxclient.New(fxSettings)
	teamName, error := client.GetTeamName()

	if error != nil {
		fmt.Print(error)
	} else {
		fmt.Println("Success ", teamName)
	}
}
