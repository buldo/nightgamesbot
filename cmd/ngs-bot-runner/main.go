package main

import (
	"fmt"
	"github.com/rockneurotiko/go-tgbot"
	"log"
	"os"

	"bitbucket.org/buldo/nightgamesbot/ngsbot"
)

const tokenString string = "botToken"
const hookURLString string = "hookUrl"
const masterPasswordString string = "masterPass"
const dbURL  string = "DATABASE_URL"

func echoHandler(bot tgbot.TgBot, msg tgbot.Message, vals []string, kvals map[string]string) *string {
	newmsg := fmt.Sprintf("[Echoed]: %s", vals[1])
	fmt.Println(newmsg)
	return &newmsg
}



func main() {
	// Конфигурация API
	infrastructureBot := tgbot.NewTgBot(os.Getenv(tokenString))
	updatesChan := make(chan tgbot.MessageWithUpdateID)
	infrastructureBot.AddMainListener(updatesChan)
	infrastructureBot.SetWebhook("")
	log.Println("Infrastructure configured")

	// Конфигурация игрового бота
	var config ngsbot.BotConfig
	config.MasterPassword = os.Getenv(masterPasswordString)
	config.PostgresConnectionString = os.Getenv(dbURL)
	teamBot := ngsbot.NewBot(config, infrastructureBot)
	go teamBot.MessageHandler(updatesChan)
	log.Println("Team bot configured")

	// Старт бота
	hookURL := os.Getenv(hookURLString)
	if hookURL == "" {
		// работаем локально вытягивая обновления
		log.Println("Starting in pooling mode")
		infrastructureBot.Start()
	} else {
		// работаем на сервере через вебхук
		log.Println("Starting in webhook mode")
		infrastructureBot.ServerStart(hookURL, "/")
	}

}
