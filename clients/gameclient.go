package clients

type GameClient interface
{
	GetCommands() []string
	ExecuteCommand()
}
