package fxclient

import (
	"net/http"
	"net/http/cookiejar"
	"errors"
	"github.com/PuerkitoBio/goquery"
	"net/url"
)

const ClientName string = "fx"

const loginPageString string = "/user/login"

type FxClientSettings struct {
	BaseUrl string	`json:"baseUrl"`
	Login string	`json:"login"`
	Password string	`json:"password"`
}

type FxClient struct {
	httpClient *http.Client
	settings FxClientSettings
}

func New(settings FxClientSettings) *FxClient{
	client := new(FxClient)

	client.settings = settings

	cookieJar, _ := cookiejar.New(nil)
	client.httpClient = &http.Client{
		Jar: cookieJar,
	}

	return client
}

func (self *FxClient)GetTeamName() (string, error) {
	resp, err := self.getWithAuthCheck(self.settings.BaseUrl + "play")
	if(err != nil) {
		return "", err
	} else {
		defer resp.Body.Close()
		doc, err := goquery.NewDocumentFromResponse(resp)

		if(err != nil) {
			return "", err
		}
		return doc.Find("p.team_name").First().Text(), nil
	}
}


//func (self *FxClient)GetOrgMessage() (string, error) {
//	err := self.chechAndAuth();
//	if(err != nil) {
//		return "", err;
//	} else {
//
//	}
//}

func isAuthenticated(resp *http.Response) bool {
	return resp.Request.URL.Path != loginPageString
}

func (self *FxClient)authenticate() (resp *http.Response, err error) {
	resp, err = self.httpClient.PostForm(self.settings.BaseUrl + loginPageString, url.Values{"email":{self.settings.Login},"pass":{self.settings.Password}})
	return
}

func (self *FxClient)getWithAuthCheck(url string) (resp *http.Response, err error) {
	// Первичный запрос
	resp, err = self.httpClient.Get(url)
	if (err != nil) {
		return nil, err
	}
	isAuth := isAuthenticated(resp)
	if(!isAuth) {
		// Если мы не залогинены, то необходимо это сделать
		resp, err = self.authenticate()
		if(err != nil) {
			return nil, err
		}

		isAuth := isAuthenticated(resp)
		if(isAuth) {
			// Если авторизация прошла, делаем тот запрос, который хотели
			return self.getWithAuthCheck(url)
		} else {
			return nil, errors.New("AuthError")
		}
	}

	return resp, nil
}

func (self *FxClient)GetCommands() []string {
	return []string{"GetTeam"}
}

func (self *FxClient)ExecuteCommand() {

}